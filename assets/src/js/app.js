import './sprite';
import './fancy';
import './mask';
import { initMmenu } from './myMmenu';
import { initCalculate } from './calculate';
import { activeTabs } from './tabs';
import { sideMenu } from './sideMenu';
import { productCatalog } from './productCatalog';
import { connectInputRange, initRange } from './range';
import { activeItem } from './activeItem';
import { initSlider } from './swiper';
import { oneActiveItem } from './oneActiveItem';
import { scrollUp } from './scrollUp';
import { changeImage } from './changeImage';
import { additionalContent } from './moreMenu';
import { mobileHideContent } from './mobileHideContent';

const links = document.querySelectorAll('.js-footer-menu-link');
activeItem(links);

const ranges = document.querySelectorAll('.js-range');
for (const item of ranges) {
  initRange(item);
}

const filterRange = document.querySelector('.js-filter-range');
connectInputRange(filterRange);

const catalogList = document.querySelector('.js-catalog-list');
productCatalog(catalogList);

const sideMenuElement = document.querySelector('.js-side-menu');
sideMenu(sideMenuElement);

const tabs = document.querySelectorAll('.js-tabs');
activeTabs(tabs);

const sliders = document.querySelectorAll('[data-swiper-type]');
for (const slider of sliders) {
  initSlider(slider);
}

const calculateElements = document.querySelectorAll('.js-calculate');
for (const item of calculateElements) {
  initCalculate(item);
}

const plants = document.querySelectorAll('.js-type-plants');
oneActiveItem(plants);

const btnUp = document.querySelector('.js-aside-btn');
scrollUp(btnUp, 540);

const mmenus = document.querySelectorAll('[data-mmenu]');
for (const item of mmenus) {
  initMmenu(item);
}

const colorImage = document.querySelector('.js-change-image');
changeImage(colorImage);

const items = document.querySelectorAll('.js-additional-content');
for (const item of items) {
  additionalContent(item);
}

const mobileHideMenus = document.querySelectorAll('[data-mobile-hide-button]');
for (const item of mobileHideMenus) {
  mobileHideContent(item);
}

import 'mmenu-js';

const options = {
  default: {
    navbar: {
      title: '',
    },
    slidingSubmenus: false,
    extensions: ['pagedim-black', 'shadow-panels', 'effect-slide-menu'],
    navbars: [
      {
        content: [],
      },
    ],
  },
};

function getOptions(name) {
  return options[name];
}

export function initMmenu(element) {
  if (!element) {
    return;
  }
  document.addEventListener('DOMContentLoaded', () => {
    const name = element.dataset.mmenu;
    const newOptions = getOptions('default');
    let template;
    if (name === 'menu') {
      template = document
        .querySelector('[data-mmenu-template="menu"]')
        .content.cloneNode(true);
    } else {
      template = document
        .querySelector('[data-mmenu-template="logo"]')
        .content.cloneNode(true);
    }
    newOptions.navbars[0].content.push(template);
    if (name === 'menu') {
      newOptions.navbar.add = false;
    } else {
      newOptions.navbar.title = element.dataset.mmenuTitle;
    }
    window.mmenu = new window.Mmenu(element, newOptions);
  });
}

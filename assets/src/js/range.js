import 'nouislider/dist/nouislider.css';
import noUiSlider from 'nouislider';

export function initRange(node) {
  if (!node) {
    return;
  }
  noUiSlider.create(node, {
    range: {
      min: 0,
      max: 100,
    },
    step: 1,
    start: [0, 100],
    connect: true,
  });
}

export function connectInputRange(node) {
  if (!node) {
    return;
  }
  const range = node.querySelector('.js-range');
  const inputs = node.querySelectorAll('[data-input]');
  range.noUiSlider.on('update', () => {
    // eslint-disable-next-line no-param-reassign
    node.querySelector('[data-input="min"]').value = Math.round(
      range.noUiSlider.get()[0]
    );
    // eslint-disable-next-line no-param-reassign
    node.querySelector('[data-input="max"]').value = Math.round(
      range.noUiSlider.get()[1]
    );
  });
  for (const input of inputs) {
    input.addEventListener('change', () => {
      const min = node.querySelector('[data-input="min"]').value;
      const max = node.querySelector('[data-input="max"]').value;
      range.noUiSlider.set([min, max]);
    });
  }
}

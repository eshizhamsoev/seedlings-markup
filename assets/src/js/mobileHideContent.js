import { listenOutsideClick } from './listenOutsideClick';

export function mobileHideContent(button) {
  if (!button) {
    return;
  }
  const name = button.dataset.mobileHideButton;
  button.addEventListener('click', () => {
    button.classList.toggle('active');
    const type = document.querySelector(`[data-mobile-hide-type=${name}]`);
    type.classList.toggle('active');
    const close = type.querySelector('.js-mobile-hide-content__close');
    const wrapper = type.querySelector('.js-mobile-hide-content__wrapper');
    if (button.classList.contains('active')) {
      const resultListener = listenOutsideClick(wrapper);
      resultListener.promise.then(() => {
        button.classList.remove('active');
        type.classList.remove('active');
      });
      close.addEventListener('click', () => {
        resultListener.close();
        button.classList.remove('active');
        type.classList.remove('active');
      });
    }
  });
}

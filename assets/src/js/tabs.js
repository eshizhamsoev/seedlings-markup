export function activeTabs(tabs) {
  for (const item of tabs) {
    const buttons = item.querySelectorAll('.js-tabs-btn');
    buttons.forEach((element) =>
      element.addEventListener('click', () => {
        const name = element.dataset.tab;
        const tabContent = item.querySelector(
          `.js-tabs-content[data-tab=${name}]`
        );
        const tabBtn = item.querySelector(`.js-tabs-btn[data-tab=${name}]`);
        if (!tabContent.classList.contains('active')) {
          const tabActiveContent = item.querySelector(
            '.js-tabs-content.active'
          );
          const tabActiveBtn = item.querySelector('.js-tabs-btn.active');
          tabActiveContent.classList.remove('active');
          tabActiveBtn.classList.remove('active');
          tabContent.classList.add('active');
          tabBtn.classList.add('active');
        }
      })
    );
  }
}
